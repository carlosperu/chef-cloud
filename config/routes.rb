Rails.application.routes.draw do

  post 'employee_token' => 'employee_token#create'
  devise_for :customers
  devise_for :employees

  namespace :admin do
    resources :restaurants, except: [:index, :create, :destroy]
    resources :locations, except: [:show] do
      resources :menus, only: [:index, :new]
    end
    resources :menus, except: [:new, :show] do
      resources :sections, only: [:new, :create]
    end
    resources :categories do
      resources :category_sections, only: [:new]
    end
    resources :category_sections, except: [:index, :show, :new] do
      resources :category_options, only: [:new]
    end
    resources :category_options, except: [:index, :show, :new]
    resources :items, except: [:show]
    resources :sections, only: [:edit, :update, :destroy] do
      resources :section_items, only: [:new, :create]
    end
    resources :section_items, only: [:edit, :update, :destroy]
    resources :employees
    resources :orders, only: [:index, :show, :update]
    resources :order_items, only: [:show]
    root to: "orders#index"
  end

  namespace :api, defaults: { format: :json } do
    namespace :v1 do
      post 'firebase_registrations' => "firebase_registrations#update"
      resources :orders, only: [:update]
      resources :locations do
        resources :orders, only: [:index]
      end
      resources :restaurants, only: [:index]
    end
  end

  resources :restaurants, only: [:show]

  get '/restaurants' => "restaurants#show", as: :customer_root
  resources :orders, only: [:new, :create]

  resources :menus, only: [:show]
  resources :locations, only: []  do
    resources :menus, only: [:index]
  end

  resources :items, only: [] do
    resources :bag_items, only: [:new]
  end
  resources :bag_items, only: [:create, :destroy]
end
