set :stage, :production

server 'www.chef-cloud.com', user: 'deploy', roles: %w{web app db}
