require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module ChefCloud
  class Application < Rails::Application
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.
    config.action_view.embed_authenticity_token_in_remote_forms = true

    # Set time zone to Pacific Time
    config.time_zone = "Pacific Time (US & Canada)"

    # Setting time zone to local
    config.active_record.default_timezone = :local

    # Only datetime and time columns will be time zone aware
    config.active_record.time_zone_aware_types = [:datetime, :time]
  end
end
