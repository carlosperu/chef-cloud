class CreateMenus < ActiveRecord::Migration[5.0]
  def change
    create_table :menus do |t|
      t.string :label, limit: 25
      t.boolean :visible, default: false
      t.time :open, default: "12:00"
      t.time :close, default: "18:00"
      t.integer :position
      t.references :location, foreign_key: true

      t.timestamps
    end
  end
end
