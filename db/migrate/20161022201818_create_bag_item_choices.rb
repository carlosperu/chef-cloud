class CreateBagItemChoices < ActiveRecord::Migration[5.0]
  def change
    create_table :bag_item_choices do |t|
      t.integer :quantity, default: 1, null: false
      t.decimal :subtotal, precision: 6, scale: 2
      t.references :category_option, foreign_key: true
      t.references :bag_item, foreign_key: true

      t.timestamps
    end
  end
end
