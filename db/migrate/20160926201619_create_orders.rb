class CreateOrders < ActiveRecord::Migration[5.0]
  def change
    create_table :orders do |t|
      t.decimal :tax, precision: 5, scale: 2
      t.text :instruction
      t.string :status, null: false
      t.decimal :subtotal, precision: 8, scale: 2
      t.integer :time
      t.references :location, foreign_key: true
      t.references :customer, foreign_key: true

      t.timestamps
    end
  end
end
