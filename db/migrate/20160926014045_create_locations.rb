class CreateLocations < ActiveRecord::Migration[5.0]
  def change
    create_table :locations do |t|
      t.string :location, limit: 100
      t.string :address, limit: 100
      t.string :city, limit: 100
      t.string :state, limit: 2
      t.string :zipcode, limit: 5
      t.string :country, limit: 50
      t.string :email, limit: 100
      t.integer :position
      t.decimal :tax, precision: 5, scale: 2
      t.integer :time
      t.text :notification_key
      t.references :restaurant, foreign_key: true


      t.timestamps
    end
  end
end
