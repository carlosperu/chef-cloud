class CreateOrderItems < ActiveRecord::Migration[5.0]
  def change
    create_table :order_items do |t|
      t.string :name, limit: 100
      t.text :description
      t.integer :quantity, default: 1, null: false
      t.decimal :price, precision: 6, scale: 2, default: 0
      t.text :instruction
      t.decimal :subtotal, precision: 7, scale: 2
      t.integer :item_id
      t.references :order, foreign_key: true

      t.timestamps
    end
  end
end
