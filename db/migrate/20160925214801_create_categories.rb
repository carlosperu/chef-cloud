class CreateCategories < ActiveRecord::Migration[5.0]
  def change
    create_table :categories do |t|
      t.string :category
      t.references :restaurant, foreign_key: true

      t.timestamps
    end
  end
end
