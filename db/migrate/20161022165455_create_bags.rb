class CreateBags < ActiveRecord::Migration[5.0]
  def change
    create_table :bags do |t|
      t.text :instruction
      t.integer :customer_id
      t.references :location, foreign_key: true

      t.timestamps
    end
  end
end
