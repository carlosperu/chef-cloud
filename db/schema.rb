# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20161024231950) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "bag_item_choices", force: :cascade do |t|
    t.integer  "quantity",                                   default: 1, null: false
    t.decimal  "subtotal",           precision: 6, scale: 2
    t.integer  "category_option_id"
    t.integer  "bag_item_id"
    t.datetime "created_at",                                             null: false
    t.datetime "updated_at",                                             null: false
    t.index ["bag_item_id"], name: "index_bag_item_choices_on_bag_item_id", using: :btree
    t.index ["category_option_id"], name: "index_bag_item_choices_on_category_option_id", using: :btree
  end

  create_table "bag_items", force: :cascade do |t|
    t.integer  "quantity",                            default: 1, null: false
    t.decimal  "subtotal",    precision: 7, scale: 2
    t.text     "instruction"
    t.integer  "bag_id"
    t.integer  "item_id"
    t.datetime "created_at",                                      null: false
    t.datetime "updated_at",                                      null: false
    t.index ["bag_id"], name: "index_bag_items_on_bag_id", using: :btree
    t.index ["item_id"], name: "index_bag_items_on_item_id", using: :btree
  end

  create_table "bags", force: :cascade do |t|
    t.text     "instruction"
    t.integer  "customer_id"
    t.integer  "location_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.index ["location_id"], name: "index_bags_on_location_id", using: :btree
  end

  create_table "categories", force: :cascade do |t|
    t.string   "category"
    t.integer  "restaurant_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.index ["restaurant_id"], name: "index_categories_on_restaurant_id", using: :btree
  end

  create_table "category_options", force: :cascade do |t|
    t.string   "name"
    t.decimal  "price",               precision: 6, scale: 2, default: "0.0"
    t.integer  "position"
    t.integer  "category_section_id"
    t.datetime "created_at",                                                  null: false
    t.datetime "updated_at",                                                  null: false
    t.index ["category_section_id"], name: "index_category_options_on_category_section_id", using: :btree
  end

  create_table "category_sections", force: :cascade do |t|
    t.text     "instruction"
    t.text     "comment"
    t.integer  "control_type"
    t.boolean  "required"
    t.integer  "position"
    t.integer  "category_id"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.index ["category_id"], name: "index_category_sections_on_category_id", using: :btree
  end

  create_table "customers", force: :cascade do |t|
    t.string   "first_name",             limit: 25
    t.string   "last_name",              limit: 25
    t.string   "phone"
    t.string   "email",                             default: "", null: false
    t.string   "encrypted_password",                default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",                     default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.datetime "created_at",                                     null: false
    t.datetime "updated_at",                                     null: false
    t.index ["email"], name: "index_customers_on_email", unique: true, using: :btree
    t.index ["reset_password_token"], name: "index_customers_on_reset_password_token", unique: true, using: :btree
  end

  create_table "employees", force: :cascade do |t|
    t.string   "first_name",             limit: 25
    t.string   "last_name",              limit: 25
    t.string   "email",                             default: "", null: false
    t.string   "encrypted_password",                default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",                     default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.integer  "restaurant_id"
    t.datetime "created_at",                                     null: false
    t.datetime "updated_at",                                     null: false
    t.index ["email"], name: "index_employees_on_email", unique: true, using: :btree
    t.index ["reset_password_token"], name: "index_employees_on_reset_password_token", unique: true, using: :btree
    t.index ["restaurant_id"], name: "index_employees_on_restaurant_id", using: :btree
  end

  create_table "items", force: :cascade do |t|
    t.string   "name",        limit: 100
    t.text     "description"
    t.decimal  "price",                   precision: 6, scale: 2, default: "0.0"
    t.integer  "category_id"
    t.datetime "created_at",                                                      null: false
    t.datetime "updated_at",                                                      null: false
    t.index ["category_id"], name: "index_items_on_category_id", using: :btree
  end

  create_table "locations", force: :cascade do |t|
    t.string   "location",         limit: 100
    t.string   "address",          limit: 100
    t.string   "city",             limit: 100
    t.string   "state",            limit: 2
    t.string   "zipcode",          limit: 5
    t.string   "country",          limit: 50
    t.string   "email",            limit: 100
    t.integer  "position"
    t.decimal  "tax",                          precision: 5, scale: 2
    t.integer  "time"
    t.text     "notification_key"
    t.integer  "restaurant_id"
    t.datetime "created_at",                                           null: false
    t.datetime "updated_at",                                           null: false
    t.index ["restaurant_id"], name: "index_locations_on_restaurant_id", using: :btree
  end

  create_table "menus", force: :cascade do |t|
    t.string   "label",       limit: 25
    t.boolean  "visible",                default: false
    t.time     "open",                   default: '2000-01-01 12:00:00'
    t.time     "close",                  default: '2000-01-01 18:00:00'
    t.integer  "position"
    t.integer  "location_id"
    t.datetime "created_at",                                             null: false
    t.datetime "updated_at",                                             null: false
    t.index ["location_id"], name: "index_menus_on_location_id", using: :btree
  end

  create_table "order_item_details", force: :cascade do |t|
    t.string   "name"
    t.decimal  "price",         precision: 6, scale: 2
    t.integer  "quantity"
    t.decimal  "subtotal",      precision: 6, scale: 2
    t.integer  "order_item_id"
    t.datetime "created_at",                            null: false
    t.datetime "updated_at",                            null: false
    t.index ["order_item_id"], name: "index_order_item_details_on_order_item_id", using: :btree
  end

  create_table "order_items", force: :cascade do |t|
    t.string   "name",        limit: 100
    t.text     "description"
    t.integer  "quantity",                                        default: 1,     null: false
    t.decimal  "price",                   precision: 6, scale: 2, default: "0.0"
    t.text     "instruction"
    t.decimal  "subtotal",                precision: 7, scale: 2
    t.integer  "item_id"
    t.integer  "order_id"
    t.datetime "created_at",                                                      null: false
    t.datetime "updated_at",                                                      null: false
    t.index ["order_id"], name: "index_order_items_on_order_id", using: :btree
  end

  create_table "orders", force: :cascade do |t|
    t.decimal  "tax",         precision: 5, scale: 2
    t.text     "instruction"
    t.string   "status",                              null: false
    t.decimal  "subtotal",    precision: 8, scale: 2
    t.integer  "time"
    t.integer  "location_id"
    t.integer  "customer_id"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.index ["customer_id"], name: "index_orders_on_customer_id", using: :btree
    t.index ["location_id"], name: "index_orders_on_location_id", using: :btree
  end

  create_table "restaurants", force: :cascade do |t|
    t.string   "name"
    t.string   "permalink"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "section_items", force: :cascade do |t|
    t.integer  "section_id"
    t.integer  "item_id"
    t.integer  "position"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["item_id"], name: "index_section_items_on_item_id", using: :btree
    t.index ["section_id"], name: "index_section_items_on_section_id", using: :btree
  end

  create_table "sections", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.integer  "position"
    t.integer  "menu_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.index ["menu_id"], name: "index_sections_on_menu_id", using: :btree
  end

  add_foreign_key "bag_item_choices", "bag_items"
  add_foreign_key "bag_item_choices", "category_options"
  add_foreign_key "bag_items", "bags"
  add_foreign_key "bag_items", "items"
  add_foreign_key "bags", "locations"
  add_foreign_key "categories", "restaurants"
  add_foreign_key "category_options", "category_sections"
  add_foreign_key "category_sections", "categories"
  add_foreign_key "employees", "restaurants"
  add_foreign_key "items", "categories"
  add_foreign_key "locations", "restaurants"
  add_foreign_key "menus", "locations"
  add_foreign_key "order_item_details", "order_items"
  add_foreign_key "order_items", "orders"
  add_foreign_key "orders", "customers"
  add_foreign_key "orders", "locations"
  add_foreign_key "section_items", "items"
  add_foreign_key "section_items", "sections"
  add_foreign_key "sections", "menus"
end
