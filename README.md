Mona's Ordering System
============================================

Dependencies
------------
The following gems were used in this project:

bootstrap-sass - 3.3.6
devise - 4.2.0
cancancan - 1.15.0
font-awesome-rails - 4.6.3.1
acts_as_list - 0.8.2
bootstrap-will_paginate - 0.0.10

Overview
------------

Integrate ordering system in Ruby on Rails to website on Wordpress.

The owner will have admin access to:
- Create online menus to the different locations of the restaurant.
- Create manager and employee users.
- Confirm the online orders.
- Change status of orders.
- Add manager users that can update menu and confirm orders.
- Add employee users that can confirm orders.

The customers will be able to:
- Create account and active them through email.
- Make takeout orders online.
- See orders history and reorder them.

Will be running by the beginning of November.

Copyright
---------
Copyright (c) 2014, CarlosCastro.me. All rights reserved.
