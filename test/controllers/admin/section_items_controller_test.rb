require 'test_helper'

class Admin::SectionItemsControllerTest < ActionDispatch::IntegrationTest
  test "should get new" do
    get section_items_new_url
    assert_response :success
  end

  test "should get create" do
    get section_items_create_url
    assert_response :success
  end

  test "should get edit" do
    get section_items_edit_url
    assert_response :success
  end

  test "should get update" do
    get section_items_update_url
    assert_response :success
  end

  test "should get destroy" do
    get section_items_destroy_url
    assert_response :success
  end

end
