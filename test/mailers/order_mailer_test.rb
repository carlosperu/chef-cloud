require 'test_helper'

class OrderMailerTest < ActionMailer::TestCase
  test "order_request" do
    mail = OrderMailer.order_request
    assert_equal "Order request", mail.subject
    assert_equal ["to@example.org"], mail.to
    assert_equal ["from@example.com"], mail.from
    assert_match "Hi", mail.body.encoded
  end

  test "order_confirmation" do
    mail = OrderMailer.order_confirmation
    assert_equal "Order Confirmation", mail.subject
    assert_equal ["to@example.org"], mail.to
    assert_equal ["from@example.com"], mail.from
    assert_match "Hi", mail.body.encoded
  end

  test "thank_you_order" do
    mail = OrderMailer.thank_you_order
    assert_equal "Thank you order", mail.subject
    assert_equal ["to@example.org"], mail.to
    assert_equal ["from@example.com"], mail.from
    assert_match "Hi", mail.body.encoded
  end

end
