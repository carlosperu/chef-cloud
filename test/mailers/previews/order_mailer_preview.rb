# Preview all emails at http://localhost:3000/rails/mailers/order_mailer
class OrderMailerPreview < ActionMailer::Preview

  # Preview this email at http://localhost:3000/rails/mailers/order_mailer/order_request
  def order_request
    OrderMailer.order_request
  end

  # Preview this email at http://localhost:3000/rails/mailers/order_mailer/order_confirmation
  def order_confirmation
    OrderMailer.order_confirmation
  end

  # Preview this email at http://localhost:3000/rails/mailers/order_mailer/thank_you_order
  def thank_you_order
    OrderMailer.thank_you_order
  end

end
