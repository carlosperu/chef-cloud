class Bag < ApplicationRecord
  belongs_to :location
  has_many :bag_items, dependent: :destroy
  has_many :bag_item_choices, through: :bag_items

  def subtotal
    bag_items.sum(:subtotal) + bag_item_choices.sum(:subtotal)
  end

  def quantity
    bag_items.sum(:quantity)
  end

end
