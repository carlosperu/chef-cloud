class Customer::ParameterSanitizer < Devise::ParameterSanitizer
  def initialize(*)
    super
    permit(:sign_up, keys: [:first_name, :last_name, :phone])
    permit(:account_update, keys: [:first_name, :last_name, :phone])
  end
end
