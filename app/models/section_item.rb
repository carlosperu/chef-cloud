class SectionItem < ApplicationRecord
  belongs_to :section
  belongs_to :item

  acts_as_list scope: :section

  validates :position, numericality: { minimum: 1, only_integer: true }

  scope :sorted, -> { order("section_items.position") }
end
