class Employee::ParameterSanitizer < Devise::ParameterSanitizer
  def initialize(*)
    super
    permit(:sign_up, keys: [:first_name, :last_name])
    permit(:account_update, keys: [:first_name, :last_name])
  end
end
