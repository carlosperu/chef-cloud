class BagItem < ApplicationRecord
  belongs_to :bag
  belongs_to :item
  has_many :bag_item_choices, inverse_of: :bag_item, dependent: :destroy
  has_many :category_options, through: :bag_item_choices

  before_save :add_subtotal

  accepts_nested_attributes_for :bag_item_choices, reject_if: :bag_item_choice_rejectable?

  validates :quantity, presence: true, numericality: true,
  length: { minimum: 1, maximum: 50 }

  private

    def add_subtotal
      self.subtotal = self.quantity * self.item.price
    end

    def bag_item_choice_rejectable?(attributes)
      attributes['category_option_id'].blank? && new_record?
    end

end
