class Order < ApplicationRecord
  belongs_to :location
  belongs_to :customer
  has_many :order_items, dependent: :destroy



  STATUS = Hash.new{ |hash, key| raise( "Key #{ key } is unknown" )}.update(
    PENDING: "Pending",
    CONFIRMED: "Confirmed",
    CANCELED: "Canceled",
    COMPLETED: "Completed",
    INCOMPLETED: "Incompleted" )

  scope :sorted, -> { order(created_at: :desc) }

  scope :latest, ->(updated_at) { where("orders.updated_at > ?", updated_at) }
  scope :recently, -> { where("orders.created_at >= ?", 1.day.ago) }

  # Less than 2 minutes
  scope :recent_only, -> { where("created_at > ?", 2.minutes.ago) }

  scope :from_location, ->(location_id) { where(location_id: location_id) }

  scope :from_restaurant, ->(restaurant_id) { joins(:location).where(locations: { restaurant_id: restaurant_id }) }

  def order_request(customer, location)
    OrderMailer.order_request(customer, location).deliver_now
  end

  def thank_you_order(customer)
    OrderMailer.thank_you_order(customer).deliver_now
  end

  def order_update
    case status
      when STATUS[:CONFIRMED]
        OrderMailer.order_confirmation(customer, location, self.time).deliver_now
      # when STATUS[:CANCELED]
      #   OrderMailer.order_confirmation(customer).deliver_now
      # when STATUS[:COMPLETED]
      #   OrderMailer.order_confirmation(customer).deliver_now
      # when STATUS[:INCOMPLETED]
      #   OrderMailer.order_confirmation(customer).deliver_now
      else
        logger.error "Unexpected status update"
    end
  end

  def created
    to_milliseconds(self.created_at)
  end

  def updated
    to_milliseconds(self.updated_at)
  end

end
