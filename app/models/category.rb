class Category < ApplicationRecord
  has_many :items
  has_many :category_sections, -> { order(:position) }, dependent: :destroy
  has_many :category_options, through: :category_sections

  validates :category, presence: true, length: { maximum: 50 }

  scope :sorted, -> { order(:position) }
end
