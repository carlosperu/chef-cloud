class OrderItemDetail < ApplicationRecord
  belongs_to :order_item, inverse_of: :order_item_details
end
