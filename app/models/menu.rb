class Menu < ApplicationRecord
  belongs_to :location
  has_many :sections, -> { order(:position) }, dependent: :destroy

  acts_as_list scope: :location

  validates :label, presence: true, length: { maximum: 25 }
  validates :position, numericality: { minimum: 1, only_integer: true }

  scope :in_location, ->(location_id) { where(location_id: location_id) }

  scope :visible, -> { where(visible: true) }

  scope :sorted, -> { order(:position) }

end
