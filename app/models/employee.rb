class Employee < ApplicationRecord
  belongs_to :restaurant
  # Include default devise modules. Others available are:
  # :registerable,, :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :recoverable,
         :rememberable, :trackable, :validatable

  alias_method :authenticate, :valid_password?

  validates :first_name, presence: true, length: { maximum: 25 }
  validates :last_name, presence: true, length: { maximum: 25 }

  def self.from_token_payload payload
    self.find payload["sub"]
  end

  scope :from_restaurant, ->(restaurant_id) { where(restaurant_id: restaurant_id) }

  # scope :staff, ->(employee) { where("id != ? and role_id != 0 and role_id <= ?", employee.id, employee.role_id) }
end
