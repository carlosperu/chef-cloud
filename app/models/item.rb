class Item < ApplicationRecord
  belongs_to :category
  has_many :section_items, dependent: :destroy
  has_many :sections, through: :section_items

  validates :name, presence: true, length: { maximum: 100 }
  validates :description, length: { maximum: 499 }
  validates :price, presence: true,
  numericality: { greater_than_or_equal_to: 0, less_than_or_equal_to: 9999 }

  scope :sorted, -> { order(:name) }

  def display
    category.nil? ? name : "#{name} - #{category.category}"
  end

  private
    # FIXME: Because this sucks
    # TODO: Find better rendering solution with html and css
    # This just determines a fixed content length for homogeneous rendering
    def curate_length
      if self.category.description
        if self.description.length > self.category.length
          self.description = self.description.truncate(self.category.length)
        elsif description.length < category.length
          self.description += " &nbsp; " * ((self.category.length - self.description.length) / 2)
        end
      end
    end

end
