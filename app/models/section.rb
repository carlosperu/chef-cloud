class Section < ApplicationRecord
  belongs_to :menu
  has_many :section_items, -> { order(:position) }, dependent: :destroy
  has_many :items, through: :section_items

  acts_as_list scope: :menu

  validates :name, presence: true, length: { maximum: 100 }
  validates :description, length: { maximum: 499 }
  validates :position, numericality: { minimum: 1, only_integer: true }

  scope :sorted, -> { order("sections.position") }
end
