class Restaurant < ApplicationRecord
  has_many :locations
  has_many :employees
  has_many :categories

  before_save :update_permalinks

  validates :name, presence: true, length: { maximum: 100 }

  def created
    to_milliseconds(self.created_at)
  end

  def updated
    to_milliseconds(self.updated_at)
  end

  private

    # Establishes the permalink based on the name
    def update_permalinks
      if name.parameterize != permalink
        self.permalink = "#{name.sub("'", "").parameterize}"
      end
    end
end
