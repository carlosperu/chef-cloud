class OrderItem < ApplicationRecord
  belongs_to :order
  has_many :order_item_details, inverse_of: :order_item, dependent: :destroy

  validates :quantity, presence: true, numericality: true,
  length: { minimum: 0, maximum: 50 }

end
