class ApplicationRecord < ActiveRecord::Base
  self.abstract_class = true

  protected

    # Get time in milliseconds unless it's nil
    def to_milliseconds(time)
      if time
        time.to_datetime.strftime('%Q')
      else
        nil
      end
    end
end
