class Api::V1::OrdersController < Api::ApplicationController

  # GET /api/v1/orders
  def index
    @orders = params[:updated_at].present? ? Order.from_location(params[:location_id]).latest(Time.at(params[:updated_at].to_i/1000)).sorted : Order.from_location(params[:location_id]).recently.sorted
    respond_to do |format|
      format.json {
        render :index
      }
    end
  end

  # PATCH/PUT /api/v1/orders/1
  def update
    respond_to do |format|
      @order = Order.find(params[:id])
      if @order.update(order_params)
        @order.order_update
        logger.info("Order was updated:" + @order.inspect)
        format.json {
          render :index, status: :ok, location: [:api, :v1, @order]
        }
      else
        format.json {
          render json: @orden.errors, status: :unprocessable_entity
        }
      end
    end
  end

  private
    # Never trust parameters from the scary internet, only allow the white list through.
    def order_params
      params.require(:order).permit(:status, :time)
    end
end
