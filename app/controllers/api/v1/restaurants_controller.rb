class Api::V1::RestaurantsController < Api::ApplicationController

  # GET /api/v1/restaurants
  def index
    @restaurant = Restaurant.find(current_employee.restaurant_id)
    respond_to do |format|
      format.json
    end
  end
end
