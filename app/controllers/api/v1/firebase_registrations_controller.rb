class Api::V1::FirebaseRegistrationsController < Api::ApplicationController
  def update
    parameters = firebase_registration_params
    if parameters[:location_id]
      project_id = "717088113082"
      new_registration_id = parameters[:new_registration_id] if !parameters[:new_registration_id].blank?
      old_registration_id = parameters[:old_registration_id] if !parameters[:old_registration_id].blank?
      @location = Location.find(params[:id])

      # This is the the Server API key
      fcm = FCM.new("AAAApvXIQbo:APA91bGAL6FpIiRDvvMjI1Y1IxkmOYJ8g5pDsb4PaORtdiHakpiL3ThSH14yNYa8ot31T4ikZiGE19ZQHYmIyvt0--A1Vj-Mkq1jSoSGUVzs195FYCk-DzWr7d7CreO89HuyO_GVilJ4")

      if old_registration_id
        response = fcm.remove(@location.id.to_s, project_id, @location.notification_key, [new_registration_id])
        logger.info "Old registration_id was removed"
      end

      if new_registration_id
        if @location.notification_key
          response = fcm.add(@location.id.to_s, project_id, @location.notification_key, [new_registration_id])
          logger.info "New registration_id was added"
        else
          response = fcm.create(@location.id.to_s, project_id, [new_registration_id])
          logger.info "New notification key was created"
          # We save the notification_key in the DB
          if response[:body][:notification_key]
            @location.notification_key = response[:body][:notification_key]
            @location.save
          end
        end
        render json: { new_registration_id: new_registration_id }, status: :ok
      else
        logger.info "Missing registration_id"
        render json: { error: "Missing registration_id" }, status: :bad_request
      end
    else
      logger.info "Missing location_id"
      render json: { error: "Missing location_id" }, status: :bad_request
    end
  end

  private
    # Never trust parameters from the scary internet, only allow the white list through.
    def firebase_registration_params
      params.require(:firebase_registration).permit(:location_id, :new_registration_id, :old_registration_id)
    end
end
