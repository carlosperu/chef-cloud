class Api::ApplicationController < ActionController::Base
  include Knock::Authenticable
  undef_method :current_employee
  before_action :authenticate_employee
end
