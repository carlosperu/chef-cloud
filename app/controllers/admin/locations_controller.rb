# Controller that handles location actions logic. It's a subclass of admin controller as it's an admin controller.
class Admin::LocationsController < Admin::AdminController
  before_action :authenticate_employee!, except: [:index]
  before_action :set_location, only: [:edit, :update, :destroy]

  layout 'admin'

  # GET /admin/locations
  # GET /admin/locations.js
  def index
    @locations = Location.sorted
    respond_to do |format|
      format.html {
        render '_index'
      }
      format.js
    end
  end

  # GET /admin/locations/new
  # GET /admin/locations/new.js
  def new
    @location = Location.new
    @locations_count = Location.count + 1
  end

  # POST /admin/locations
  # POST /admin/locations.js
  def create
    @location = Location.new(location_params)
    @location.restaurant_id = current_employee.restaurant_id
    respond_to do |format|
      if @location.save
        restaurant = @location.restaurant\
        format.html { redirect_to @location, notice: 'Location was successfully created.' }
        format.js {
          @locations = Location.sorted
          flash[:notice] = "Location was successfully created."
          render :index
        }
      else
        format.html {
          render :new
        }
        format.js {
          @locations_count = Location.count + 1
          render :new
        }
      end
    end
  end

  # GET /admin/locations/1/edit
  # GET /admin/locations/1/edit.js
  def edit
    @locations_count = Location.count
  end

  # PATCH/PUT /admin/locations/1
  # PATCH/PUT /admin/locations/1.js
  def update
    respond_to do |format|
      if @location.update(location_params)
        format.html { redirect_to @location, notice: 'Location was successfully updated.' }
        format.js {
          @locations = Location.sorted
          flash[:notice] = "Location was successfully updated."
          render :index
        }
      else
        format.html { render :edit }
        format.js {
          @locations_count = Location.count
          render :edit
        }
      end
    end
  end

  # DELETE /admin/locations/1.js
  def destroy
    @location.destroy
    respond_to do |format|
      format.html { redirect_to locations_url, notice: 'Location was successfully deleted.' }
      format.js {
        @locations = Location.sorted
        flash[:notice] = "Location was successfully deleted."
        render :index
      }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_location
      @location = Location.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def location_params
      params.require(:location).permit(:location, :address, :city, :state, :zipcode, :country, :email, :email_confirmation, :position, :tax, :time)
    end

end
