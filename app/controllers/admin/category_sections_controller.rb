# Controller that handles category sestion actions logic. It's a subclass of admin controller as it's an admin controller.
class Admin::CategorySectionsController < Admin::AdminController
  before_action :set_category_section, only: [:edit, :update, :destroy]

  layout 'admin'

  # GET /admin/category/1/category_sections/new
  def new
    @category_section = CategorySection.new(category_id: params[:category_id])
    @category_sections_count = CategorySection.where(category_id: params[:category_id]).count + 1
    respond_to do |format|
      format.js
    end
  end

  # POST /admin/category_sections
  # POST /admin/category_sections.json
  def create
    @category_section = CategorySection.new(category_section_params)
    respond_to do |format|
      if @category_section.save
        format.html { redirect_to category_url(Category.find(params[:category_id])),
          notice: 'The category was added successfully to the Menu.' }
        format.js {
          @category_sections = CategorySection.where(category_id: @category_section.category_id).sorted
          flash[:notice] = "Section for the category was successfully created."
          render :index
        }
      else
        @category_sections_count = CategorySection.where(category_id: @category_section.category_id).count + 1
        format.html { render :new }
        format.js { render :new }
      end
    end
  end

  # GET /admin/category_sections/1/edit
  def edit
    @category_sections_count = CategorySection.where(category_id: @category_section.category_id).count
    respond_to do |format|
      format.html
      format.js
    end
  end

  # PATCH/PUT /admin/category_sections/1
  # PATCH/PUT /admin/category_sections/1.json
  def update
    respond_to do |format|
      if @category_section.update(category_section_params)
        format.html { redirect_to category_url(@category_section.category_id),
          notice: 'Category section was successfully updated.' }
        format.js {
          @category_sections = CategorySection.where(category_id: @category_section.category_id).order(:position)
          flash[:notice] = "Section for the category was successfully updated."
          render :index
        }
      else
        @category_sections_count = CategorySection.where(category_id: @category_section.category_id).count
        format.html { render :edit }
        format.js { render :edit }
      end
    end
  end

  # DELETE /admin/category_sections/1
  # DELETE /admin/category_sections/1.json
  def destroy
    @category_section.destroy
    respond_to do |format|
      format.html { redirect_to category_url(@category_section.category_id), notice: 'Category section was successfully destroyed.' }
      format.js {
        @category_sections = CategorySection.where(category_id: @category_section.category_id).sorted
        @category_section = @category_sections.first
        flash[:notice] = "Section for the category was successfully deleted."
        render :index
      }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_category_section
      @category_section = CategorySection.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def category_section_params
      params.require(:category_section).
      permit(:category_id, :control_type, :required, :position, :instruction, :comment)
    end
end
