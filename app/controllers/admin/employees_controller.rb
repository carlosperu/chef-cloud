# Controller that handles employee actions logic. It's a subclass of admin controller as it's an admin controller.
class Admin::EmployeesController < Admin::AdminController
  before_action :set_employee, only: [:edit, :update, :destroy]

  layout 'admin'

  def index
    # @employees = Employee.staff(current_employee)
    @employees = Employee.from_restaurant(current_employee.restaurant_id)
    render '_index'
  end

  def new
    @employee = Employee.new
    respond_to do |format|
      format.html
      format.js
    end
  end

  def create
    @employee = Employee.new(employee_params)
    @employee.restaurant_id = current_employee.restaurant_id
    respond_to do |format|
      # Use only if confirmable
      # @employee.skip_confirmation!
      if @employee.save
        format.html { redirect_to @employee, notice: 'Employee was successfully created.' }
        format.js {
          # @employees = Employee.staff(current_employee)
          @employees = Employee.from_restaurant(current_employee.restaurant_id)
          flash[:notice] = "Employee was successfully created."
          render :index
        }
      else
        format.html { render :new }
        format.js { render :new }
      end
    end
  end

  def edit
    respond_to do |format|
      format.html
      format.js
    end
  end

  def update
    remove_password
    respond_to do |format|
      # Use only if confirmable
      # @employee.skip_reconfirmation!
      if @employee.update(employee_params)
        format.html {
          redirect_to @employee, notice: 'Employee was successfully updated.'
        }
        format.js {
          # @employees = Employee.staff(current_employee)
          @employees = Employee.from_restaurant(current_employee.restaurant_id)
          flash[:notice] = "Employee was successfully updated."
          render :index
        }
      else
        format.html { render :edit }
        format.js { render :edit }
      end
    end
  end

  def destroy
    if @employee.destroy
      respond_to do |format|
        format.html { redirect_to action: :index, notice: 'Location was successfully deleted.' }
        format.js {
          # @employees = Employee.staff(current_employee)
          flash[:notice] = "User was successfully deleted."
          render :index
        }
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_employee
      @employee = Employee.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def employee_params
      params.require(:employee).permit(:first_name, :last_name, :phone, :email, :password, :password_confirmation)#, :role_id)
    end

    def remove_password
      params[:employee].delete(:password) if params[:employee][:password].blank?
      params[:employee].
      delete(:password_confirmation) if params[:employee][:password].blank? &&
      params[:employee][:password_confirmation].blank?
    end
end
