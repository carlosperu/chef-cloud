# Controller that handles restaurant actions logic. It's a subclass of admin controller as it's an admin controller.
class Admin::RestaurantsController < Admin::AdminController
  before_action :set_restaurant, only: [:show, :edit, :update, :destroy]

  layout 'admin'

  # GET /admin/restaurants/1
  def show
    @locations = Location.sorted
    respond_to do |format|
      format.html {
        render '_index'
      }
      format.js {
        render 'application/index', path: "locations"
      }
    end
  end

  # GET /restaurants/new
  def new
    @restaurant = Restaurant.new
  end

  # GET /admin/restaurants/1/edit
  def edit
  end

  # PATCH/PUT /admin/restaurants/1
  def update
    respond_to do |format|
      if @restaurant.update(restaurant_params)
        format.html { redirect_to @restaurant, notice: 'Restaurant was successfully updated.' }
        format.json { render :show, status: :ok, location: @restaurant }
      else
        format.html { render :edit }
        format.json { render json: @restaurant.errors, status: :unprocessable_entity }
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_restaurant
      puts "ver"
      puts current_employee.inspect
      @restaurant = Restaurant.find(current_employee.restaurant_id)
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def restaurant_params
      params.require(:restaurant).permit(:name, :permalink)
    end
end
