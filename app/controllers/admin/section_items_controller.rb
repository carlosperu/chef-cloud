# Controller that handles section item actions logic. It's a subclass of admin controller as it's an admin controller.
class Admin::SectionItemsController < Admin::AdminController
  before_action :set_section_item, only: [:edit, :update, :destroy]

  layout 'admin'

  # GET /admin/sections/1/section_items/new
  def new
    @section_item = SectionItem.new(section_id: params[:section_id])
    @items = Item.where.not(id: SectionItem.select(:item_id).
    where(section_id: params[:section_id])).sorted
    @section_items_count = SectionItem.where(section_id: params[:section_id]).count + 1
    respond_to do |format|
      format.html
      format.js
    end
  end

  # POST /admin/section_items
  # POST /admin/section_items.json
  def create
    @section_item = SectionItem.new(section_item_params)
    @section_item.section_id = params[:section_id]
    respond_to do |format|
      if @section_item.save
        format.html { redirect_to menu_url(@section_item.section.menu_id),
          notice: 'The item was added successfully to the Menu.' }
        format.js {
          @section_items = SectionItem.where(section_id: @section_item.section_id).order(:position)
          flash[:notice] = "Section item was successfully created."
          render :index
        }
      else
        format.html { render :new }
        format.js {
          @items = Item.where.not(id: SectionItem.select(:item_id).
          where(section_id: @section_item.section_id)).sorted
          @section_items_count = SectionItem.where(section_id: @section_item.section_id).count + 1
          render :new
        }
      end
    end
  end

  # GET /admin/section_items/1/edit
  def edit
    @items = Item.where.not(id: SectionItem.select(:item_id).
    where(section_id: @section_item.section_id).
    where.not(item_id: @section_item.item_id)).sorted
    @section_items_count = SectionItem.where(section_id: @section_item.section_id).count
    respond_to do |format|
      format.html
      format.js
    end
  end

  # PATCH/PUT /admin/section_items/1
  # PATCH/PUT /admin/section_items/1.json
  def update
    respond_to do |format|
      if @section_item.update(section_item_params)
        format.html { redirect_to menu_url(@section_item.section.menu_id),
          notice: 'Section item was successfully updated.' }
        format.js {
          @section_items = SectionItem.where(section_id: @section_item.section_id).order(:position)
          flash[:notice] = "Section item was successfully updated."
          render :index
        }
      else
        format.html { render :edit }
        format.js {
          @items = Item.where.not(id: SectionItem.select(:item_id).
          where(section_id: @section_item.section_id).
          where.not(item_id: @section_item.item_id)).sorted
          @section_items_count = SectionItem.where(section_id: @section_item.section_id).count
          render :edit
        }
      end
    end
  end

  # DELETE /admin/section_items/1
  # DELETE /admin/section_items/1.json
  def destroy
    @section_item.destroy
    respond_to do |format|
      format.html { redirect_to menu_url(@section_item.section.menu_id), notice: 'Location was successfully destroyed.' }
      format.js {
        @section_items = SectionItem.where(section_id: @section_item.section_id).order(:position)
        flash[:notice] = "Section item was successfully deleted."
        render :index
      }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_section_item
      @section_item = SectionItem.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def section_item_params
      params.require(:section_item).permit(:section_id, :item_id, :position)
    end
end
