# Controller that handles category actions logic. It's a subclass of admin controller as it's an admin controller.
class Admin::CategoriesController < Admin::AdminController
  before_action :set_category, only: [:show, :edit, :update, :destroy]

  layout 'admin'

  # GET /admin/categories
  # GET /admin/categories.js
  def index
    @categories = Category.all
    respond_to do |format|
      format.html {
        render '_index'
      }
      format.js
    end
  end

  # GET /admin/categories/1
  def show
  end

  # GET /admin/categories/new
  # GET /admin/categories/new.js
  def new
    @category = Category.new
    respond_to do |format|
      format.html
      format.js
    end
  end

  # POST /admin/categories
  # POST /admin/categories.js
  def create
    @category = Category.new(category_params)
    @category.restaurant_id = current_employee.restaurant_id
    respond_to do |format|
      if @category.save
        format.html { redirect_to categories_url, notice: 'Category was successfully created.' }
        format.js {
          @categories = Category.all
          flash[:notice] = "Category was successfully created."
          render :index
        }
      else
        format.html { render :new }
        format.js { render :new }
      end
    end
  end

  # GET /items/1/edit
  # GET /items/1/edit.js
  def edit
    respond_to do |format|
      format.html
      format.js
    end
  end

  # PATCH/PUT /admin/categories/1
  # PATCH/PUT /admin/categories/1.js
  def update
    respond_to do |format|
      if @category.update(category_params)
        format.html { redirect_to @category, notice: 'Category was successfully updated.' }
        format.js {
          @categories = Category.all
          flash[:notice] = "Category was successfully updated."
          render :index
        }
      else
        format.html { render :edit }
        format.js { render :edit }
      end
    end
  end

  # DELETE /admin/categories/1
  # DELETE /admin/categories/1.js
  def destroy
    @category.destroy
    respond_to do |format|
      format.html { redirect_to items_url, notice: 'Item was successfully deleted.' }
      format.js {
        @categories = Category.all
        flash[:notice] = "Category was successfully deleted."
        render :index
      }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_category
      @category = Category.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def category_params
      params.require(:category).permit(:category)
    end
end
