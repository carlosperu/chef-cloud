# Controller that handles order actions logic. It's a subclass of admin controller as it's an admin controller.
class Admin::OrdersController < Admin::AdminController
  before_action :set_order, only: [:show, :update]

  layout 'admin'

  # GET /admin/orders
  def index
    @orders = params[:location_id].present? ? Order.where(location_id: params[:location_id]).sorted : Order.sorted
    respond_to do |format|
      format.html {
        render '_index'
      }
    end
  end

  # GET /admin/orders/1
  def show
    respond_to do |format|
      format.html
    end
  end

  # PATCH/PUT /admin/orders/1
  def update
    respond_to do |format|
      if @order.update(order_params)
        @order.order_update
        format.html {
          redirect_to admin_orders_url, notice: 'Order status was successfully updated.'
        }
      else
        format.html {
          redirect_to admin_orders_url, notice: 'Order status wan\'t able to get updated.'
        }
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_order
      @order = Order.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def order_params
      params.require(:order).permit(:status)
    end
end
