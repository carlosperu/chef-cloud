# Controller that handles section actions logic. It's a subclass of admin controller as it's an admin controller.
class Admin::SectionsController < Admin::AdminController
  before_action :set_section, only: [:edit, :update, :destroy]

  layout 'admin'

  # GET /admin/menus/1/sections/new
  def new
    @section = Section.new(menu_id: params[:menu_id])
    @sections_count = Section.where(menu_id: params[:menu_id]).count + 1
    respond_to do |format|
      format.html
      format.js
    end
  end

  # POST /admin/sections
  # POST /admin/sections.json
  def create
    @section = Section.new(section_params)
    @section.menu_id = params[:menu_id]
    respond_to do |format|
      if @section.save
        format.html { redirect_to menu_url(@section.menu_id), notice: 'Section was successfully created.' }
        format.js {
          @sections = Section.where(menu_id: @section.menu_id).order(:position)
          flash[:notice] = "Section was successfully updated."
          render :index
        }
      else
        format.html { render :new }
        format.js {
          @sections_count = Section.where(menu_id: @menu.id).count + 1
          render :new
        }
      end
    end
  end

  # GET /admin/sections/1/edit
  def edit
    @sections_count = Section.where(menu_id: @section.menu_id).count
    respond_to do |format|
      format.html
      format.js
    end
  end

  # PATCH/PUT /admin/sections/1
  # PATCH/PUT /admin/sections/1.json
  def update
    respond_to do |format|
      if @section.update(section_params)
        format.html { redirect_to menu_url(@section.menu_id), notice: 'Section was successfully updated.' }
        format.js {
          @sections = Section.where(menu_id: @section.menu_id).order(:position)
          flash[:notice] = "Section was successfully updated."
          render :index
        }
      else
        format.html { render :edit }
        format.js { @sections_count = Section.where(menu_id: @section.menu_id).count
          render :edit
        }
      end
    end
  end

  # DELETE /admin/sections/1
  # DELETE /admin/sections/1.json
  def destroy
    @section.destroy
    respond_to do |format|
      format.html { redirect_to menu_url(@section.menu_id), notice: 'Section was successfully destroyed.' }
      format.js {
        @sections = Section.where(menu_id: @section.menu_id).order(:position)
        flash[:notice] = "Section was successfully deleted."
        render :index
      }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_section
      @section = Section.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def section_params
      params.require(:section).permit(:menu_id, :name, :description, :position)
    end
end
