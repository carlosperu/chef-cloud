# Controller that handles category options actions logic. It's a subclass of admin controller as it's an admin controller.
class Admin::CategoryOptionsController < Admin::AdminController
  before_action :set_category_option, only: [:edit, :update, :destroy]

  layout 'admin'

  # GET /admin/category_sections/1/category_options/new
  def new
    @category_option = CategoryOption.new(category_section_id: params[:category_section_id])
    @category_options_count = CategoryOption.where(category_section_id: params[:category_section_id]).count + 1
    respond_to do |format|
      format.js
    end
  end

  # POST /admin/category_options
  # POST /admin/category_options.json
  def create
    respond_to do |format|
      @category_option = CategoryOption.new(category_option_params)
      if @category_option.save
        format.html { redirect_to category_url(@category_section.category_id),
          notice: 'Category option was successfully created.' }
        format.js  {
          @category_options = CategoryOption.where(category_section_id: @category_option.category_section_id).order(:position)
          flash[:notice] = "Option choice for the category was successfully created."
          render :index
        }
      else
        @category_options_count = CategoryOption.where(category_section_id: @category_section.id).count + 1
        format.html { render :new }
        format.js { render :new }
      end
    end
  end

  # GET /admin/category_options/1/edit
  def edit
    @category_options_count = CategoryOption.where(category_section_id: @category_option.category_section_id).count
    respond_to do |format|
      format.html
      format.js
    end
  end

  # PATCH/PUT /admin/category_options/1
  # PATCH/PUT /admin/category_options/1.json
  def update
    respond_to do |format|
      if @category_option.update(category_option_params)
        format.html { redirect_to @category_option, notice: 'Category option was successfully updated.' }
        format.js  {
          @category_options = CategoryOption.where(category_section_id: @category_option.category_section_id).order(:position)
          flash[:notice] = "Option choice for the category was successfully updated."
          render :index
        }
      else
        @category_options_count = CategoryOption.where(category_section_id: @category_option.category_section_id).count
        format.html { render :edit }
        format.js { render :edit }
      end
    end
  end

  # DELETE /admin/category_options/1
  # DELETE /admin/category_options/1.json
  def destroy
    @category_option.destroy
    respond_to do |format|
      format.html { redirect_to category_options_url, notice: 'Category option was successfully destroyed.' }
      format.js {
        @category_options = CategoryOption.where(category_section_id: @category_option.category_section_id).order(:position)
        flash[:notice] = "Option choice for the category was successfully deleted."
        render :index
      }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_category_option
      @category_option = CategoryOption.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def category_option_params
      params.require(:category_option).permit(:name, :price, :position, :category_section_id)
    end
end
