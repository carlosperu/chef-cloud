# Controller that handles menu actions logic. It's a subclass of admin controller as it's an admin controller.
class Admin::MenusController < Admin::AdminController
  before_action :set_menu, only: [:edit, :update, :destroy]

  layout 'admin'

  # GET /admin/locations/1/menus/
  # GET /admin/menus/
  def index
    # Get sorted locations
    @locations = Location.from_restaurant(current_employee.restaurant_id).sorted
    # Get location id if sent through the params
    location_id = params[:location_id]
    # If location provided retrieve object. Otherwise just grab first location
    if location_id
      @location = Location.find(location_id)
    else
      @location = @locations.first
    end
    @menus = Menu.in_location(@location.id)
    respond_to do |format|
      format.html {
        render '_index'
      }
      format.js
    end
  end

  # GET /admin/locations/1/menus/new
  def new
    @menu = Menu.new(location_id: params[:location_id])
    @menus_count = Menu.where(location_id: params[:location_id]).count + 1
    respond_to do |format|
      format.js {
        @locations = Location.sorted
      }
    end
  end

  # POST /admin/menus
  # POST /admin/menus.json
  def create
    @menu = Menu.new(menu_params)
    respond_to do |format|
      if @menu.save
        format.html { redirect_to @menu, notice: 'Menu was successfully created.' }
        format.js {
            @menus = Menu.where(location_id: @menu.location_id).sorted
            @locations = Location.sorted
            flash[:notice] = "Menu was successfully created."
            render :index
        }
      else
        format.html { render :new }
        format.js {
          @menus_count = Menu.where(location_id: @menu.location_id).count + 1
          @locations = Location.sorted
          render :new
        }
      end
    end
  end

  # GET /admin/menus/1/edit
  def edit
    @menus_count = Menu.where(location_id: @menu.location_id).count
    respond_to do |format|
      format.js {
        @locations = Location.sorted
      }
    end
  end

  # PATCH/PUT /admin/menus/1
  # PATCH/PUT /admin/menus/1.json
  def update
    respond_to do |format|
      if @menu.update(menu_params)
        format.html { redirect_to @menu, notice: 'Menu was successfully updated.' }
        format.js {
          @menus = Menu.where(location_id: @menu.location_id).sorted
          @locations = Location.sorted
          flash[:notice] = "Menu was successfully updated."
          render :index
        }
      else
        format.html { render :edit }
        format.js {
          @menus_count = Menu.where(location_id: @menu.location_id).count
          @locations = Location.sorted
          render :edit
        }
      end
    end
  end

  # DELETE /admin/menus/1.js
  def destroy
    @menu.destroy
    respond_to do |format|
      format.html { redirect_to location_url(@menu.location_id), notice: 'Menu was successfully destroyed.' }
      format.js {
        @menus = Menu.where(location_id: @menu.location_id).sorted
        @locations = Location.all
        @menu = @menus.first
        flash[:notice] = "Menu was successfully deleted."
        render :index
      }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_menu
      @menu = Menu.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def menu_params
      params.require(:menu).permit(:location_id, :label, :visible, :open, :close, :position)
    end

end
