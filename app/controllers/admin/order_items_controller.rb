# Controller that handles order item actions logic. It's a subclass of admin controller as it's an admin controller.
class Admin::OrderItemsController < Admin::AdminController

  layout 'admin'

  # GET /admin/order_items/1
  # GET /admin/order_items/1.json
  def show
    @order_item = OrderItem.find(params[:id])
  end

end
