# Controller that handles item actions logic. It's a subclass of admin controller as it's an admin controller.
class Admin::ItemsController < Admin::AdminController
  before_action :set_item, only: [:edit, :update, :destroy]

  layout 'admin'

  # GET /admin/items
  # GET /admin/items.js
  def index
    @items = Item.sorted
    respond_to do |format|
      format.html {
        render '_index'
      }
      format.js
    end
  end

  # GET /admin/items/new
  # GET /admin/items/new.js
  def new
    @item = Item.new
    respond_to do |format|
      format.html
      format.js {
        @categories = Category.all
      }
    end
  end

  # GET /admin/items/1/edit
  # GET /admin/items/1/edit.js
  def edit
    respond_to do |format|
      format.html
      format.js {
        @categories = Category.all
      }
    end
  end

  # POST /admin/items
  # POST /admin/items.js
  def create
    @item = Item.new(item_params)
    respond_to do |format|
      if @item.save
        format.html { redirect_to items_url, notice: 'Item was successfully created.' }
        format.js {
          @items = Item.all.sorted
          flash[:notice] = "Item was successfully created."
          render :index
        }
      else
        format.html { render :new }
        format.js { render :new }
      end
    end
  end

  # PATCH/PUT /admin/items/1
  # PATCH/PUT /admin/items/1.js
  def update
    respond_to do |format|
      if @item.update(item_params)
        format.html { redirect_to @item, notice: 'Item was successfully updated.' }
        format.js {
          @items = Item.all.sorted
          flash[:notice] = "Item was successfully updated."
          render :index
        }
      else
        format.html { render :edit }
        format.js { render :edit }
      end
    end
  end

  # DELETE /admin/items/1
  # DELETE /admin/items/1.js
  def destroy
    @item.destroy
    respond_to do |format|
      format.html { redirect_to items_url, notice: 'Item was successfully deleted.' }
      format.js {
        @items = Item.all.sorted
        flash[:notice] = "Item was successfully deleted."
        render :index
      }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_item
      @item = Item.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def item_params
      params.require(:item).permit(:name, :description, :price, :category_id)
    end
end
