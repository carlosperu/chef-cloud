# Base controller class for the admin controllers
class Admin::AdminController < ApplicationController
  before_action :authenticate_employee!

end
