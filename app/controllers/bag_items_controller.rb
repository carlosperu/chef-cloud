class BagItemsController < ApplicationController

  # GET /items/1/bag_items/new
  def new
    # Retrieve bag from session
    if @bag.nil?
      if customer_signed_in?
        @bag = Bag.find_by_customer_id(current_customer.id)
      elsif session[:bag_id]
        @bag = Bag.find(session[:bag_id])
        session[:bag_id] = nil if @bag.nil?
      end
    end

    # Retrieve item and menu from params
    @item = Item.find(params[:item_id])

    # If bag still nil, create one. Otherwise, just update the location id
    if @bag.nil?
      @bag = Bag.new(location_id: session[:location_id])
      @bag.customer_id = current_customer.id if customer_signed_in?
      @bag.save
    else
    #   @bag.update_attributes(location_id: session[:location_id])
    end
    @bag_item = BagItem.new(bag_id: @bag.id, item_id: @item.id)
    session[:bag_id] = @bag.id
    session[:bag_item_id] = @bag_item.id
    @bag_item_choices = []
    @item.category.category_sections.count.times do
      @bag_item_choices << BagItemChoice.new
    end
  end

  # POST /bag_items
  def create
    @bag = Bag.find(session[:bag_id])
    @bag_item = @bag.bag_items.build(bag_item_params)
    respond_to do |format|
      if @bag_item.save
        format.js {
          flash[:notice] = "Item was successfully added to your Bag."
          render 'bags/show'
        }
      else
        format.js {
          @item = Item.find(@bag_item.item_id)
          @bag_item_choices = []
          @item.category.category_sections.count.times do
            @bag_item_choices << BagItemChoice.new
          end
          render :new
        }
      end
    end
  end

  # DELETE /bag_items/1
  def destroy
    @bag_item = BagItem.find(params[:id])
    @bag_item.destroy
    flash[:notice] = "Item was successfully removed from your Bag."
    @bag = Bag.find(session[:bag_id])
    respond_to do |format|
      format.html {
        if @bag.bag_items.size > 0
          redirect_to new_order_path
        else
          redirect_to restaurant_url(session[:permalink])
        end
      }
      format.js {
        render 'bags/show'
      }
    end
  end

  private
    def bag_item_params
      params.require(:bag_item).permit(:quantity, :instruction, :item_id, bag_item_choices_attributes: [:category_option_id])
    end

    def bag_item_choices_params
      params.require(:bag_item).permit(:bag_item_choices_attributes)
    end
end
