class MenusController < ApplicationController

  # GET /locations/1/menus.js
  def index
    @menus = Menu.where(location_id: params[:location_id]).sorted.visible
    @menu = @menus.first
    session[:menu_id] = @menu.id
    if params[:location_id] != session[:location_id]
      session[:location_id] = params[:location_id]
      sync_bag
    end
    respond_to do |format|
      format.js
    end
  end

  # GET /menus/1.js
  def show
    @menu = Menu.find(params[:id])
    @menus = Menu.where(location_id: @menu.location_id).sorted.visible
    session[:menu_id] = @menu.id
    if @menu.location_id != session[:location_id]
      session[:location_id] = @menu.location_id
      sync_bag
    end
    puts "show carajo"
    p @menu
    p session[:menu_id]
    respond_to do |format|
      format.js { render :index }
    end
  end

  private

  def sync_bag
    if session[:bag_id]
      @bag = Bag.find(session[:bag_id])
      if @bag.location_id != session[:location_id]
        @bag.update(location_id: session[:location_id])
      end
    end
  end

end
