class OrdersController < ApplicationController
  before_action :authenticate_customer!

  # GET /orders/1
  def new
    @bag = Bag.find(session[:bag_id])
    @location = Location.find(@bag.location_id)
    @order = Order.new(location_id: @location.id, subtotal: @bag.subtotal)
  end

  # POST /orders
  def create
    @bag = Bag.find(session[:bag_id])
    @location = Location.find(@bag.location_id)
    @order = Order.new(order_params)
    @order.tax = (@order.subtotal * @order.location.tax / 100)
    @order.status = Order::STATUS[:PENDING]
    @order.customer_id = current_customer.id
    @order.time = @location.time
    # We proceed with the content of the order if the order gets successfully
    # saved in the DB.
    if @order.save
      @bag.bag_items.each do |bag_item|
        item = bag_item.item
        # We don't create the order item directly in the DB. Instead, we keep it
        # in memory while we add the details of the item. Once the details are
        # added, we insert everything in the DB.
        order_item = OrderItem.new(name: item.name, description: item.description, quantity: bag_item.quantity, price: item.price, instruction: bag_item.instruction, subtotal: bag_item.subtotal, item_id: item.id, order_id: @order.id)
        bag_item.bag_item_choices.each do |bag_item_choice|
          category_option = bag_item_choice.category_option
          order_item.order_item_details.build(name: category_option.name,
          price: category_option.price, quantity: bag_item_choice.quantity, subtotal: bag_item_choice.subtotal)
        end
        # Order item and its order item details are saved in the DB.
        order_item.save
        logger.info "Order was creaated with id #{@order.id}"
      end
      session[:bag_id] = nil
      @order.order_request(current_customer, @location)
      @order.thank_you_order(current_customer)
      @employee = Employee.where(restaurant_id: @location.restaurant_id).take
      fcm = FCM.new("AAAApvXIQbo:APA91bGAL6FpIiRDvvMjI1Y1IxkmOYJ8g5pDsb4PaORtdiHakpiL3ThSH14yNYa8ot31T4ikZiGE19ZQHYmIyvt0--A1Vj-Mkq1jSoSGUVzs195FYCk-DzWr7d7CreO89HuyO_GVilJ4")
      response = fcm.send_with_notification_key(@location.notification_key,
                  data: {
                    sound: "star_wars",
                    order_id: @order.id,
                    location_id: @location.id,
                    restaurant_id: @location.restaurant_id
                  },
                  collapse_key: "new_order")
      logger.info "Firebase message sent with notification_key: #{@location.notification_key}"
      @bag.destroy
      # Use the thank you value to display a message in restaurant index
      session[:thank_you] = true
      redirect_to restaurant_url(@location.restaurant.permalink)
    else
      render :new
    end

  end

  private

    # Never trust parameters from the scary internet, only allow the white list through.
    def order_params
      params.require(:order).permit(:instruction, :location_id, :subtotal, :tax)
    end
end
