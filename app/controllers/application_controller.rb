class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_action :configure_permitted_parameters, if: :devise_controller?

  layout :layout_by_resource
  #
  # rescue_from CanCan::AccessDenied do |exception|
  #   flash[:error] = exception.message
  #   redirect_to root_url
  # end
  #
  def after_sign_in_path_for(resource)
    # check for the class of the object to determine what type it is
    if resource.class == Customer
      super
    elsif resource.class ==  Employee
      admin_root_path
    end
  end

  def after_sign_out_path_for(resource_or_scope)
    # check for the class of the object to determine what type it is
    request.referrer
  end

  protected
    def configure_permitted_parameters
      def devise_parameter_sanitizer
        if resource_class == Customer
          Customer::ParameterSanitizer.new(Customer, :customer, params)
        elsif resource_class == Employee
          Employee::ParameterSanitizer.new(Employee, :employee, params)
        end
      end
    end

    def layout_by_resource
      if devise_controller? && resource_name == :employee
        "admin"
      else
        "application"
      end
    end
  #
  #   def configure_permitted_parameters
  #     devise_parameter_sanitizer.permit(:sign_up, keys: [:first_name, :last_name, :phone, :role_id])
  #     devise_parameter_sanitizer.permit(:account_update, keys: [:first_name, :last_name, :phone, :role_id])
  #   end
end
