class RestaurantsController < ApplicationController

  def index
  end

  # GET /restaurants/1
  # GET /restaurants/1.json
  def show
    if (session[:thank_you])
      session[:thank_you] = nil
      render 'thank_you'
    else
      @restaurant = Restaurant.find_by_permalink(params[:id] ? params[:id] : "monas-burger")
      session[:restaurant_id] = @restaurant.id
      session[:permalink] = @restaurant.permalink
      # Retrieve bag from current customer
      if  customer_signed_in?
        @bag = Bag.find_by_customer_id(current_customer.id)
        session[:bag_id] = @bag.id if !@bag.nil? && @bag.location.restaurant_id == @restaurant.id
      end

      # If the current customer does not have a bag, we look in the session
      if @bag.nil? && session[:bag_id]
        @bag = Bag.find_by_id(session[:bag_id])
        # TODO: Check if if having a query that joins the restaurant improves performance
        @bag = nil if @bag && @bag.location.restaurant_id != @restaurant.id
        if @bag.nil?
          # if the bag is nil, that bag_id is useless.
          session[:bag_id] = nil
        elsif @bag && @bag.customer_id.nil? && customer_signed_in?
          # If there's a customer currently signed in, let's assign the bag to the customer
          @bag.update(customer_id: current_customer.id)
        end
      end

      @locations = Location.from_restaurant(@restaurant.id).with_visible_menus.sorted
      # We determine the location and menu based on session information.
      # First from the session menu id if any.
      # Second from the session location id if any.
      # Third from the restaurant locations.
      # Otherwise, an exception is raised because it means that the restaurant
      # has no locations.
      # TODO: Raise exception
      if session[:menu_id]
        @menu = Menu.find(session[:menu_id])
        @location = @menu.location
        @menus = @location.menus.visible
      elsif session[:location_id]
        @location = Location.find(session[:location_id])
        @menus = Menu.where(location_id: @location.id).sorted.visible
        @menu = @menus.first if !@menus.nil?
      elsif @locations && @locations.length > 0
        @location = @locations.first
        @menus = Menu.where(location_id: @location.id).sorted.visible
        @menu = @menus.first if !@menus.nil?
      end
      session[:location_id] = @location.id if @location
      session[:menu_id] = @menu.id if @menu
      render :show
    end
  end

end
