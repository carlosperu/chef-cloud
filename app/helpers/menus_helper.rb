module MenusHelper

  # Determines whether the group should be render.
  def should_render_group(length, count)
    mind_count = length % 2 == 0 ? length/2 - 1 : length/2
    count == mind_count
  end
end
