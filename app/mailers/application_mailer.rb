class ApplicationMailer < ActionMailer::Base
  default from: 'monasoakgrove@gmail.com'
  layout 'mailer'
end
