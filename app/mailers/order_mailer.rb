class OrderMailer < ApplicationMailer

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.order_mailer.order_request.subject
  #
  def order_request(customer, location)
    @greeting = "Hi from #{customer.first_name} #{customer.last_name}"

    mail to: location.email, subject: "New Order Placed!"
  end

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.order_mailer.order_confirmation.subject
  #
  def order_confirmation(customer, time, location)
    @greeting = "Hi #{customer.first_name} #{customer.last_name}"
    @time = time
    @address = "#{location.address}, #{location.city}, #{location.state} #{location.zipcode}"

    mail to: customer.email, subject: "Your order is now confirmed."
  end

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.order_mailer.thank_you_order.subject
  #
  def thank_you_order(customer)
    @greeting = "Hi #{customer.first_name} #{customer.last_name}"

    mail to: customer.email, subject: "Thank you"
  end
end
