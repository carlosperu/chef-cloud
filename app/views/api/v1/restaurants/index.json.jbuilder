  json.extract! @restaurant, :id, :name, :permalink, :updated, :created
  json.locations @restaurant.locations do |location|
    json.extract! location, :id, :location, :address, :city, :state, :country, :zipcode, :tax, :email, :updated, :created
  end
  json.url api_v1_restaurants_url
