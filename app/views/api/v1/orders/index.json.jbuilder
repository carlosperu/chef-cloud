json.array!(@orders) do |order|
  json.extract! order, :id, :status, :instruction, :subtotal, :tax, :time, :location_id, :updated, :created
  json.extract! order.customer, :first_name, :last_name, :phone, :email
  json.order_items order.order_items do |order_item|
    json.extract! order_item, :id, :name, :description, :price, :quantity, :subtotal, :instruction, :item_id
    json.order_item_details order_item.order_item_details do |order_item_detail|
      json.extract! order_item_detail, :id, :name, :price, :quantity, :subtotal
    end
  end
end
