function listenToMenusChange() {
  $("#menu_id").change(function() {
    // Request script that replaces the menu content
    // TODO: try use the event object!!
    $.getScript("/menus/" + $("#menu_id").val())
      .fail(function(jqxhr, settings, exception) {
        console.log("Something went wrong: " + exception);
    });
  });
}

function listenToLocationsChange() {
  $("#menu_location_id").change(function() {
    // Request script that replaces the menu dropdown and the menu content
    // TODO: try use the event object!!
    $.getScript("/locations/" + $("#menu_location_id").val() + "/menus")
      .fail(function(jqxhr, settings, exception) {
        console.log("Something went wrong: " + exception);
    });
  });
}

function setUpListeners() {
  $("#submit").click(function() {
    window.location.href=$("input:radio[name=location]:checked").val();
  });
  listenToLocationsChange();
  listenToMenusChange();
  setUpSearchBox();
}

function setUpSearchBox() {
  var search = $(".search");
  var searchBox = search.find(".search-box");
  searchBox.focusout(function() {
    search.hide();
  });
  $(".search-icon").click(function(e) {
    e.preventDefault();
    search.toggle();
    searchBox.focus();
  });
}

function initTooltip() {
  $(function () {
    $('[data-toggle="tooltip"]').tooltip()
  });
}

// For Rails 5 (Turbolinks 5) page:load becomes turbolinks:load and will
// be even fired on initial load. So we can just do the following:
$(document).on('turbolinks:load', function() {
  setUpListeners();
  initTooltip();
});
